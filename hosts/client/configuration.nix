{
  self,
  pkgs,
  ...
}: {
  boot = {
    supportedFilesystems = ["exfat" "ext4" "btrfs" "vfat" "fat"];
  };
  zramSwap.enable = true;

  networking = {
    firewall = {enable = true;};
    networkmanager = {
      enable = true;
      wifi = {
        powersave = true;
        backend = "iwd";
      };
    };
    wireless.enable = false;
  };

  console = {keyMap = "de";};

  i18n = {defaultLocale = "de_DE.UTF-8";};

  time.timeZone = "Europe/Berlin";

  hardware = {
    pulseaudio.enable = true;
    enableAllFirmware = true;
  };

  nix = {
    package = pkgs.nixVersions.stable;
    settings.experimental-features = ["nix-command" "flakes"];
  };

  nixpkgs.config.allowUnfree = true;

  environment = {
    systemPackages =
      (with pkgs; [
        agda
        chez
        darktable
        emacs
        firefox
        geogebra6
        gcc
        gimp
        inkscape
        racket
        texlive.combined.scheme-small
      ])
      ++ (with self.packages; [
        python-with-packages
        vscode-with-packages
      ]);

    etc = {
      "NetworkManager/system-connections/Mathecamp-Intranet.nmconnection" = {
        mode = "0600";
        text = builtins.readFile ./secrets/wifi-intranet-config;
      };
    };
  };

  fonts = {
    enableDefaultPackages = true;
  };

  users = {
    mutableUsers = false;
    users.gregor = {
      description = "Gregor";
      initialPassword = "mathecamp";
      isNormalUser = true;
      shell = pkgs.fish;
      group = "users";
      extraGroups = ["video" "audio" "adbusers" "plugdev" "davfs2" "netdev" "wheel"];
    };
  };

  programs = {fish = {enable = true;};};

  services = {
    journald.extraConfig = ''
      Storage=volatile
      SystemMaxUse=2M
    '';

    xserver = {
      enable = true;
      layout = "de";
      xkbVariant = "nodeadkeys";
      desktopManager = {
        plasma5.enable = true;
        xterm.enable = false;
      };

      displayManager = {
        autoLogin.enable = true;
        autoLogin.user = "gregor";
      };

      libinput = {
        enable = true;
        touchpad.middleEmulation = true;
      };
    };
  };

  system.stateVersion = "23.05";

  boot = {
    postBootCommands = ''
          mkdir -p /home/gregor/Desktop /home/cloud

          cd /home/gregor
          cp --no-preserve=mode -nrT ${self.packages.home-skeleton} .
          ln -s /home/cloud /home/gregor/Desktop/cloud

          chown -R gregor:users /home/gregor /home/cloud
          chmod 0700 /home/gregor/.ssh
          chmod 0600 /home/gregor/.ssh/id_rsa

          cat <<EOF > /cloud
      #!${pkgs.bash}/bin/bash

      while :; do
          ${pkgs.sshfs}/bin/sshfs -f -o ssh_command=${pkgs.openssh}/bin/ssh -o StrictHostKeyChecking=accept-new -o ServerAliveInterval=15 -o idmap=user -o reconnect gregor@cloud.lan:/home/gregor/current/cloud /home/cloud
          sleep 5
      done
      EOF
          chmod +x /cloud

          ${pkgs.busybox}/bin/su gregor -c '/cloud &'
    '';
  };
}

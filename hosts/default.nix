{
  inputs,
  withSystem,
  ...
}: {
  flake.nixosConfigurations = withSystem "x86_64-linux" ({
    system,
    self',
    ...
  }: {
    client-iso = inputs.nixpkgs.lib.nixosSystem {
      inherit system;

      modules = [
        ./client/configuration.nix
        (
          {modulesPath, ...}: {
            imports = [
              (modulesPath + "/installer/cd-dvd/installation-cd-base.nix")
            ];
            nix.registry.nixpkgs.flake = inputs.nixpkgs;
            _module.args.self = self';
          }
        )
      ];
    };

    client-vm = inputs.nixpkgs.lib.nixosSystem {
      inherit system;

      modules = [
        ./client/configuration.nix
        (
          {modulesPath, ...}: {
            imports = [
              (modulesPath + "/virtualisation/qemu-vm.nix")
            ];
            _module.args.self = self';
            nix.registry.nixpkgs.flake = inputs.nixpkgs;
          }
        )
      ];
    };

    client-lenovo = inputs.nixpkgs.lib.nixosSystem {
      inherit system;

      modules = [
        ./client/configuration.nix
        ./client/hardware-configuration-lenovo.nix
        {
          disko.devices = import ./client/disks.nix {
            disks = ["/dev/nvme0n1"];
            lib = inputs.nixpkgs.lib;
          };
          imports = [
            inputs.disko.nixosModules.disko
          ];
          _module.args.self = self';
          nix.registry.nixpkgs.flake = inputs.nixpkgs;
        }
      ];
    };

    cloud = inputs.nixpkgs.lib.nixosSystem {
      inherit system;

      modules = [
        ./cloud/configuration.nix
        ./cloud/hardware-configuration.nix
        {
          disko.devices = import ./client/disks.nix {
            disks = ["/dev/sda"];
            lib = inputs.nixpkgs.lib;
          };
          imports = [
            inputs.disko.nixosModules.disko
          ];
          nix.registry.nixpkgs.flake = inputs.nixpkgs;
        }
      ];
    };
  });
}

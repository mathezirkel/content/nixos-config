{pkgs, ...}: {
  imports = [
    ./hardware-configuration.nix
  ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "cloud";
  networking.networkmanager.enable = true;
  networking.hostId = "81a7894c";

  time.timeZone = "Europe/Berlin";

  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    useXkbConfig = true;
  };

  services.xserver.enable = true;

  services.zfs = {
    autoScrub.enable = true;
    autoSnapshot.enable = true;
    trim.enable = true;
  };

  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;

  services.xserver.layout = "de";

  users.users.gregor = {
    isNormalUser = true;
    initialHashedPassword = "$6$sIG1EqgEgZo.YIyr$NqlpAjodImN/TQ2AKbr59LzUtYdzyTt9SNGiuetZzby.ss.w2cthwITlojV9MyBbD5tdO/OiVs9kgHdN2WrlA1";
    openssh.authorizedKeys.keyFiles = [../../pkgs/home-skeleton/files/.ssh/id_rsa.pub];
  };

  users.users.admin = {
    isNormalUser = true;
    extraGroups = ["wheel"];
    initialHashedPassword = "$6$0p7qmlAXLyDPsJMd$Oq69GR5elBJlcduOdtFOe1fhNcFiCXm1Hh/rHVmlyIB1YNDwI1raIE.TDTAf/5DcbFrqOtZzb3p9HYz0MinzN/";
    shell = pkgs.fish;
  };

  users.mutableUsers = false;

  programs.fish.enable = true;

  nix = {
    package = pkgs.nixVersions.stable;
    settings.experimental-features = ["nix-command" "flakes"];
  };

  environment.systemPackages = with pkgs; [
    neovim
    firefox
    git
    wget
  ];

  services.openssh = {
    enable = true;
  };

  system.stateVersion = "23.11";
}

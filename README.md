# Nix Flake for the Mathecamp

This repository contains a flake that defines the [NixOS](https://nixos.org)
configuration used at the mathecamp for live systems.

## Building

For building the various system configuration you need an installation of nix
with flakes enabled.

### QEMU-VM

```shell
nix build .#nixosConfigurations.client-vm.config.system.build.vm
```

This build a qemu image and a script that starts qemu with the required
arguments. The script can be found in `result/bin/`.

### ISO-Image

```shell
nix build .#nixosConfigurations.client-iso.config.system.build.isoImage
```

This builds the client configuration and creates an iso-image that can be found
in `result/`.

### Applications

This flake packages some applications in `pkgs/` that are (not yet) available in
upstream [nixpkgs](https://github.com/NixOS/nixpkgs/). Those can be build with

```shell
nix build .#application-name
```

and a shell with the package available can be started via

```shell
nix shell .#application-name
```

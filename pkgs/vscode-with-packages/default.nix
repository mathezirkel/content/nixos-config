{pkgs}:
pkgs.vscode-with-extensions.override {
  vscode = pkgs.vscodium;
  vscodeExtensions = with pkgs.vscode-extensions; [
    ms-python.python
    ritwickdey.liveserver
  ];
}

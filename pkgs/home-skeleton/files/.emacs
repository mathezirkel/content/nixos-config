(load-file (let ((coding-system-for-read 'utf-8)) (shell-command-to-string "agda-mode locate")))

(setq-default indent-tabs-mode nil)

(custom-set-variables
  '(xterm-mouse-mode t)
  '(inhibit-startup-screen t))

(require 'evil)
(setq evil-default-state 'emacs)
(setq evil-want-fine-undo 't)

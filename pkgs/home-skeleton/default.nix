{stdenvNoCC}:
stdenvNoCC.mkDerivation {
  name = "home-skeleton";
  src = ./.;

  installPhase = ''
    runHook preInstall

    cp -r files $out

    runHook postInstall
  '';
}

{
  stdenv,
  lib,
  fetchurl,
  dpkg,
  autoPatchelfHook,
  xorg,
  ncurses5,
  zlib,
}:
stdenv.mkDerivation rec {
  pname = "reduce";
  version = "6547";
  # releases are published under snapshot_${date}
  date = "2023-03-08";

  src = fetchurl {
    url = "mirror://sourceforge/reduce-algebra/snapshot_${date}/linux64/reduce-complete_${version}_amd64.deb";
    hash = "sha256-zeSSVuHJpPQfoQM5IU6gATpQ9c9nSnjaX5UA/UtawCM=";
  };

  nativeBuildInputs = [
    autoPatchelfHook
    dpkg
  ];

  buildInputs = [
    xorg.libX11
    xorg.libXrandr
    xorg.libXcursor
    xorg.libXft

    ncurses5
    zlib
    stdenv.cc.cc.lib
  ];

  dontUnpack = true;

  installPhase = ''
    runHook preInstall

    mkdir -p $out/bin
    dpkg -x $src $out
    cp -r $out/usr/* $out/
    rm -r $out/usr

    runHook postInstall
  '';

  meta = {
    homepage = "https://reduce-algebra.sourceforge.io/";
    license = lib.licenses.bsd0;
    description = "portable general-purpose computer algebra system";
    platforms = lib.platforms.linux;
  };
}

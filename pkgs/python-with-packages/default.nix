{pkgs}:
pkgs.python311.withPackages (ps:
    with ps; [
      beautifulsoup4
      cryptography
      func-timeout
      matplotlib
      numpy
      pandas
      pygame
      requests
      scrapy
      tkinter
    ])

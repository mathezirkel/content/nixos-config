{
  stdenvNoCC,
  lib,
  fetchFromGitHub,
}:
stdenvNoCC.mkDerivation {
  pname = "metamath-database";
  version = "2022-06-28";

  src = fetchFromGitHub {
    owner = "metamath";
    repo = "set.mm";
    rev = "f6dc365cea78ab57fd9442b50dc8e42095e2f298";
    hash = "sha256-t7ncqo15B0WVujx/KMcvnaywBo0OEO7JQCyG2Vz5FZU=";
  };

  installPhase = ''
    runHook preInstall

    mkdir -p $out
    cp -r $src/* $out

    runHook postInstall
  '';

  meta = with lib; {
    description = "Metamath source file for logic and set theory";
    homepage = "https://github.com/metamath/set.mm";
    license = licenses.cc0;
    platforms = platforms.unix;
  };
}

{
  stdenvNoCC,
  lib,
  fetchurl,
  jre,
  makeDesktopItem,
  makeWrapper,
}: let
  icon = fetchurl {
    name = "kara.png";
    url = "https://www.swisseduc.ch/informatik/karatojava/kara/icons/kara-large.png";
    hash = "sha256-9npIQ84N0O6j1kc2+LzdOsAqUjDK9KKRNgviqX0Wtd8=";
  };
in
  stdenvNoCC.mkDerivation rec {
    name = "kara";
    version = "20230723201157";
    src = fetchurl {
      url = "https://web.archive.org/web/${version}/https://www.swisseduc.ch/informatik/karatojava/kara/classes/java17/kara.jar";
      hash = "sha256-6w6bumi/efoSHdJHz2kzycw34Q0jEblLEM6usJrWB0E=";
    };

    dontUnpack = true;

    nativeBuildInputs = [makeWrapper];

    installPhase = ''
      runHook preInstall

      mkdir -pv $out/share/java $out/bin
      cp ${src} $out/share/java/${name}-${version}.jar
      makeWrapper ${jre}/bin/java $out/bin/kara \
        --add-flags "-jar $out/share/java/${name}-${version}.jar"
      ln -s ${desktopItem}/share/applications $out/share/applications
      mkdir -p $out/share/pixmaps
      ln -s ${icon} $out/share/pixmaps/kara.png

      runHook postInstall
    '';

    desktopItem = makeDesktopItem {
      inherit name;
      desktopName = "Kara";
      exec = name;
      icon = name;
      genericName = meta.description;
      categories = [
        "Education"
        "ComputerScience"
      ];
    };

    meta = with lib; {
      description = "Programmieren mit endlichen Automaten";
      longDescription = ''
        Kara ist ein Marienkäfer, der in einer einfachen Welt lebt. Er kann programmiert
        werden und so diverse Aufgaben erledigen, zum Beispiel Kleeblätter sammeln.
        Karas Programme sind endliche Automaten und werden in einer grafischen
        Entwicklungsumgebung erstellt.'';
      homepage = "https://www.swisseduc.ch/informatik/karatojava/kara/index.html";
      license = licenses.unfree;
      # jre is marked as unsecure on some platforms, causing "nix flake check"
      # to fail
      platforms = ["x86_64-linux"];
    };
  }

{
  stdenv,
  lib,
  fetchurl,
  xorg,
}:
stdenv.mkDerivation rec {
  pname = "pmars";
  version = "0.9.2";

  src = fetchurl {
    url = "mirror://sourceforge/corewar/pMARS/${version}/${pname}-${version}.tar.gz";
    hash = "sha256-Kuhjjsa2U1DPn4E6YOM4tTTf+njD6F8YI6K+6LfCCjQ=";
  };

  sourceRoot = "${pname}-${version}/src";

  buildInputs = [
    xorg.libX11
  ];

  installPhase = ''
    runHook preInstall

    mkdir -p $out/bin
    mv pmars $out/bin/

    runHook postInstall
  '';

  hardeningDisable = ["format"];

  meta = with lib; {
    homepage = "http://www.koth.org/pmars/";
    downloadPage = "https://sourceforge.net/projects/corewar/";
    license = licenses.gpl2;
    description = "Portable MARS is the official Core War simulator of the ICWS";
    platforms = lib.platforms.linux;
  };
}

{
  systems = ["x86_64-linux" "aarch64-linux"];

  perSystem = {pkgs, ...}: {
    packages = {
      home-skeleton = pkgs.callPackage ./home-skeleton {};

      kara = pkgs.callPackage ./kara {};

      pmars = pkgs.callPackage ./pmars {};

      python-with-packages = pkgs.callPackage ./python-with-packages {};

      reduce = pkgs.callPackage ./reduce {};

      setmm = pkgs.callPackage ./metamath-database {};

      vscode-with-packages = pkgs.callPackage ./vscode-with-packages {};
    };
  };
}
